from django.db import models

from oneragtime.investors.models import Investor

from .model_utils import InvestmentType


# Create your models here.
class Investment(models.Model):
    investor_id = models.ForeignKey(Investor, on_delete=models.SET_NULL, null=True)
    startup_name = models.CharField(max_length=100)
    invested_ammount = models.IntegerField()
    percentage_fees = models.IntegerField()
    date_added = models.DateTimeField()
    fees_type = models.CharField(choices=InvestmentType.choices(), max_length=15)
