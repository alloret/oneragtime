from oneragtime.core.model_utils import BetterChoices


class InvestmentType(BetterChoices):
    UPFRONT = "upfront"
    YEARLY = "yearly"
