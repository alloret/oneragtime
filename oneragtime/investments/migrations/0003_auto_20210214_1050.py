# Generated by Django 3.0.12 on 2021-02-14 10:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('investors', '0004_auto_20210214_1023'),
        ('investments', '0002_auto_20210214_1047'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='investment',
            name='investor',
        ),
        migrations.AddField(
            model_name='investment',
            name='investor_id',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='investors.Investor'),
        ),
    ]
