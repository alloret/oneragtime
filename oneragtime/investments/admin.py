from django.contrib import admin

# Register your models here.
from import_export import fields
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import Investment


class RessourceInvestment(resources.ModelResource):
    class Meta:
        model = Investment


@admin.register(Investment)
class InvestmentAdmin(ImportExportModelAdmin):
    list_display = ["id", "startup_name", "fees_type"]
    resource_class = RessourceInvestment
