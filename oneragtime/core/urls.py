from django.urls import path

from . import views

app_name = "core"
urlpatterns = [
    path("confirm_delete/", view=views.confirm_delete, name="confirm_delete"),
    path("delete_db/", view=views.delete_db, name="delete_db"),
    path("load_initial/", view=views.load_initial, name="load_initial"),
]
