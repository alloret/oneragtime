from django.core.management import call_command
from django.http.response import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string

from oneragtime.bills.models import Bill
from oneragtime.cashcalls.models import CashCall
from oneragtime.investments.models import Investment
from oneragtime.investors.models import Investor


def confirm_delete(request):
    bills = len(Bill.objects.all())
    cashcalls = len(CashCall.objects.all())
    invesments = len(Investment.objects.all())
    investors = len(Investor.objects.all())
    modal_confirm = render_to_string(
        "pages/modal_delete.html",
        {"bills": bills, "cashcalls": cashcalls, "invesments": invesments, "investors": investors},
        request,
    )
    return JsonResponse({"modal": modal_confirm})


def delete_db(request):
    call_command("flush", interactive=False)
    return JsonResponse({"status": "ok"})


def load_initial(request):
    call_command("loaddata", "investor")
    call_command("loaddata", "investments")
    return JsonResponse({"status": "ok"})
