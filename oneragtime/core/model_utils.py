from enum import Enum


class BetterChoices(Enum):
    @classmethod
    def choices(cls):
        return [(tag.name, tag.value) for tag in cls]

    @classmethod
    def get_value_from_name(cls, name):
        return dict(cls.choices()).get(name, None)
