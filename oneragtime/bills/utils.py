from datetime import datetime
from datetime import timedelta

import pytz
from dateutil.relativedelta import relativedelta
from django.utils import timezone

from oneragtime.investments.model_utils import InvestmentType
from oneragtime.investments.models import Investment

from .models import Bill

utc = pytz.UTC


def generate_bills_upfront():
    investments = Investment.objects.filter(fees_type=InvestmentType.UPFRONT.value)
    now = timezone.localtime(timezone.now())
    n_generated = 0
    for investment in investments:
        n_generated += generate_bill_timedelta(investment, 5 * 53, now)
    return n_generated


def generate_bills_yearly():
    investments = Investment.objects.filter(fees_type=InvestmentType.YEARLY.value)
    now = timezone.localtime(timezone.now())
    n_generated = 0
    for investment in investments:
        n_generated += generate_bill_timedelta(investment, 53, now)
    return n_generated


def generate_bill_timedelta(investment, timedelta_weeks, now):
    n_generated = 0
    start = investment.date_added
    end = start + timedelta(weeks=timedelta_weeks)
    while True:
        if not Bill.objects.filter(investment_id=investment, date_added__gte=start, date_added__lte=end).exists():
            bill = Bill.objects.create(
                investor_id=investment.investor_id,
                investment_id=investment,
                fees_type=investment.fees_type,
                date_added=start,
                fees_amount=compute_amount(investment, start),
            )
            bill.save()
            n_generated += 1
        if end > now:
            return n_generated
        start = end
        end += timedelta(weeks=timedelta_weeks)


def compute_amount(investment, start):
    full_cost = investment.invested_ammount * investment.percentage_fees / 100
    if investment.fees_type == InvestmentType.UPFRONT.value:
        return float(full_cost) * 5

    elif investment.fees_type == InvestmentType.YEARLY.value:
        year_num = relativedelta(start, investment.date_added).years + 1
        last_day_year = datetime(investment.date_added.year, 12, 31).replace(tzinfo=utc)
        if investment.date_added < datetime(2019, 4, 1).replace(tzinfo=utc):
            # First year
            if year_num == 0:
                days = (last_day_year - investment.date_added).days
                return days / 365 * full_cost
            else:
                return full_cost
        else:
            # First Year
            if year_num == 1:
                days = (last_day_year - investment.date_added).days
                return round(days / 365 * full_cost, 3)
            # Second Year
            elif year_num == 2:
                return full_cost
            # Third Year
            elif year_num == 3:
                return investment.invested_ammount * ((investment.percentage_fees / 100) - 0.2)
            # Fourth
            elif year_num == 4:
                return investment.invested_ammount * ((investment.percentage_fees / 100) - 0.5)
            # All the other years
            else:
                return investment.invested_ammount * ((investment.percentage_fees / 100) - 1)
