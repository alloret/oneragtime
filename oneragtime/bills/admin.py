from django.contrib import admin
from django.contrib.admin import register

from .models import Bill


# Register your models here.
@register(Bill)
class BillAdmin(admin.ModelAdmin):
    list_display = ["fees_amount", "date_added", "fees_type", "investor_id", "investment_id"]
    pass
