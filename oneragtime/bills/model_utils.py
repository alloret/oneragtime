from oneragtime.core.model_utils import BetterChoices


class BillStatus(BetterChoices):
    CREATED = "created"
    VALIDATED = "validated"
