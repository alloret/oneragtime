from django.http import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string

from oneragtime.cashcalls.models import CashCall
from oneragtime.investors.models import Investor

from .forms import BillForm
from .model_utils import BillStatus
from .models import Bill
from .utils import generate_bills_upfront
from .utils import generate_bills_yearly


def list_bills(request):
    bills = Bill.objects.all()
    return render(request, "bills/list.html", {"bills": bills})


def generate_bills(request):
    upfront = generate_bills_upfront()
    yearly = generate_bills_yearly()
    modal_content = render_to_string("bills/modal_generate.html", {"upfront": upfront, "yearly": yearly}, request)
    return JsonResponse({"status": "ok", "modal": modal_content})


def filtered_bills(request):
    form = BillForm()
    return render(request, "bills/filtered.html", {"form": form})


def filter_bills(request):
    bills = Bill.objects.filter(investor_id=8)
    form = BillForm(data=request.POST)
    if form.is_valid():
        bills = form.cleaned_data[1]
        bills_html = render_to_string("bills/datatable.html", {"bills": bills}, request)
        return JsonResponse({"status": "ok", "bills_html": bills_html})
    return JsonResponse({"status": "nok", "bills_html": "NOT VALID"})


def get_bill(request, bill_pk):
    bill = Bill.objects.get(pk=bill_pk)
    bill_html = render_to_string("bills/modal_validate.html", {"bill": bill}, request)
    return JsonResponse({"status": "ok", "bill": bill_html})


def validate_bill(request, bill_pk):
    bill = Bill.objects.get(pk=bill_pk)
    bill.status = BillStatus.VALIDATED.value
    bill.save(update_fields=["status"])
    CashCall.upsert_cashcall(bill)
    bills = Bill.objects.filter(investor_id=bill.investor_id)
    bills_html = render_to_string("bills/datatable.html", {"bills": bills}, request)
    return JsonResponse({"status": "ok", "bills_html": bills_html})
