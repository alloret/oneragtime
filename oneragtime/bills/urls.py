from django.urls import path

from oneragtime.bills.views import filter_bills
from oneragtime.bills.views import filtered_bills
from oneragtime.bills.views import generate_bills
from oneragtime.bills.views import get_bill
from oneragtime.bills.views import list_bills
from oneragtime.bills.views import validate_bill

app_name = "bills"
urlpatterns = [
    path("generate/", view=generate_bills, name="generate"),
    path("list/", view=list_bills, name="list"),
    path("filtered/", view=filtered_bills, name="filtered"),
    path("filter_by_investor/", view=filter_bills, name="filter_by_investor"),
    path("bill/<int:bill_pk>/", view=get_bill, name="bill"),
    path("validate/<int:bill_pk>/", view=validate_bill, name="validate"),
]
