from django.db import models

from oneragtime.investments.model_utils import InvestmentType
from oneragtime.investments.models import Investment
from oneragtime.investors.models import Investor

from .model_utils import BillStatus


# Create your models here.
class Bill(models.Model):
    investor_id = models.ForeignKey(Investor, on_delete=models.SET_NULL, null=True)
    investment_id = models.ForeignKey(Investment, on_delete=models.SET_NULL, null=True)
    fees_amount = models.FloatField()
    date_added = models.DateTimeField()
    fees_type = models.CharField(choices=InvestmentType.choices(), max_length=15)
    status = models.CharField(choices=BillStatus.choices(), max_length=15, default=BillStatus.CREATED.value)
