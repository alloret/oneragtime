from django import forms
from django.forms import fields

from oneragtime.investors.models import Investor

from .models import Bill


class BillForm(forms.Form):
    investor = forms.ModelChoiceField(
        queryset=Investor.objects.all(), widget=forms.Select(attrs={"onChange": "refresh()"}), label="Investor"
    )

    # class Meta:
    #     model = Bill
    #     fields = ["investor_id"]

    def clean(self):
        cleaned_data = super().clean()
        investor = Investor.objects.get(name=cleaned_data.get("investor"))
        bills = Bill.objects.filter(investor_id=investor)
        return cleaned_data, bills
