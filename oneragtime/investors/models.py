from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


# Create your models here.
class Investor(models.Model):
    name = models.CharField(max_length=100)
    adress = models.CharField(max_length=100)
    credit = models.CharField(max_length=100)
    phone = models.CharField(max_length=100)
    email = models.EmailField()

    def __str__(self):
        return self.name
