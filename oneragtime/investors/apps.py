from django.apps import AppConfig


class InvestorsConfig(AppConfig):
    name = "oneragtime.investors"
