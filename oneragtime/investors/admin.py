from django.contrib import admin

# Register your models here.
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import Investor


class RessourceInvestor(resources.ModelResource):
    class Meta:
        model = Investor


@admin.register(Investor)
class InvestorAdmin(ImportExportModelAdmin):
    list_display = ["id", "name"]
    resource_class = RessourceInvestor
