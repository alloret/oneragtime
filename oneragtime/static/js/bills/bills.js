$(document).ready(function () {
  $("#table_id").DataTable();

  $("#generate-bills").click(function () {
    $("#loading").css("display", "block");
    $.ajax({
      url: generateUrl,
      success: function (data) {
        $("#modals").html(data.modal);
        $("#modals").modal("show");
        $("#loading").css("display", "none");
      },
    });
  });
});
