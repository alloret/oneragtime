from django.urls import path

from . import views

app_name = "cashcalls"
urlpatterns = [
    path("list/", view=views.list_cashcalls, name="list"),
    path("send/<int:pk_cashcall>/", view=views.send_cashcall, name="send"),
]
