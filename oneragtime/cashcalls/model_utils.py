from oneragtime.core.model_utils import BetterChoices


class InvoiceStatus(BetterChoices):
    UNVALIDATED = "validated"
    VALIDATED = "validated"
    SENT = "sent"
    PAID = "paid"
    OVERDUE = "overdue"
