from django.db import models

from oneragtime.bills.models import Bill
from oneragtime.bills.models import Investor

from .model_utils import InvoiceStatus


# Create your models here.
class CashCall(models.Model):
    total_amount = models.FloatField(default=0)
    IBAN = models.CharField(max_length=50, null=True)
    email_send = models.EmailField(null=True)
    invoice_status = models.CharField(
        choices=InvoiceStatus.choices(), max_length=15, default=InvoiceStatus.VALIDATED.value
    )
    bills = models.ManyToManyField(Bill, verbose_name="Bills")
    year = models.IntegerField()
    investor = models.ForeignKey(Investor, on_delete=models.SET_NULL, null=True)

    @classmethod
    def upsert_cashcall(cls, bill: Bill):
        year = bill.date_added.year
        investor = bill.investor_id
        cashcall, _ = cls.objects.get_or_create(
            year=year, investor=investor, invoice_status=InvoiceStatus.VALIDATED.value
        )
        cashcall.bills.add(bill)
        cashcall.total_amount = cashcall.compute_amount()
        cashcall.save()

    def compute_amount(self):
        total_amount = 0
        for bill in self.bills.all():
            total_amount += bill.fees_amount
        return total_amount
