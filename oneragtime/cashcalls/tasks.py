from __future__ import absolute_import
from __future__ import unicode_literals

from celery import shared_task
from celery.utils.log import get_task_logger

# from smartrd.mongo.mongo import mongo

# from smartrd.core.ta

# celery -A influence.core worker -B -l INFO
logger = get_task_logger(__name__)


@shared_task(bind=True)
def overdue_cashcalls(self):

    return "no overdue calls"
