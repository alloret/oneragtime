from django.http import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string

from .model_utils import InvoiceStatus
from .models import CashCall


# Create your views here.
def list_cashcalls(request):
    cashcalls = CashCall.objects.all()
    return render(request, "cashcalls/list_cashcalls.html", {"cashcalls": cashcalls})


def send_cashcall(request, pk_cashcall):
    cashcall = CashCall.objects.get(pk=pk_cashcall)
    cashcall.invoice_status = InvoiceStatus.SENT.value
    cashcall.save(update_fields=["invoice_status"])
    cashcall_html = render_to_string("cashcalls/cashcall_card.html", {"cashcall": cashcall}, request)
    cashcall_id = f"cashcall-{cashcall.id}"
    return JsonResponse({"status": "ok", "cashcall_html": cashcall_html, "cascall_id": cashcall_id})
