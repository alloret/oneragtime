from django.apps import AppConfig


class CashcallsConfig(AppConfig):
    name = "oneragtime.cashcalls"
