from django.contrib import admin
from django.contrib.admin import register

from .models import CashCall


# Register your models here.
@register(CashCall)
class CashCallAdmin(admin.ModelAdmin):
    # list_display = ["fees_amount", "date_added", "fees_type", "investor_id", "investment_id"]
    pass
